package org.sdp8.katra.backend.api.security;

import java.util.List;

public interface IUserProfile {
    String getName();
    List<String> getContactNumbers();
    void addContactNumbers(String... contactNumbers);
}
