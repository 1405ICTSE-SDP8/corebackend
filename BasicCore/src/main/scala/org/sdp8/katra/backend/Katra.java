package org.sdp8.katra.backend;

import org.sdp8.katra.modules.billing.BillingConfiguration;
import org.sdp8.katra.modules.bookings.BookingConfiguration;
import org.sdp8.katra.modules.checking.CheckingConfig;
import org.sdp8.katra.modules.customers.CustConfiguration;
import org.sdp8.katra.modules.datastore.hybrid.DBConfiguration;
import org.sdp8.katra.modules.facilities.FacilityConfiguration;
import org.sdp8.katra.modules.rooms.RoomsConfiguration;
import org.sdp8.katra.modules.users.UsersConfiguration;
import org.springframework.boot.SpringApplication;

import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Katra {
    public static void main(String[] args) throws ClassNotFoundException {
        String[] configs = new String[]{};
        List<Object> sources = new ArrayList<>(Arrays.asList(
                Application.class,
                DBConfiguration.class,
                UsersConfiguration.class,
                CustConfiguration.class,
                RoomsConfiguration.class,
                FacilityConfiguration.class,
                BookingConfiguration.class,
                CheckingConfig.class,
                BillingConfiguration.class
        ));
        for (String s : configs)
            sources.add(Class.forName(s));

        SpringApplication app = new SpringApplication(sources.toArray());
        app.run(args);
    }
}
