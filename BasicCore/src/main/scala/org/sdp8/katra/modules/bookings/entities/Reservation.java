package org.sdp8.katra.modules.bookings.entities;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.sdp8.katra.backend.util.ISO2Instant;
import org.sdp8.katra.backend.util.ISO2LocalDate;
import org.sdp8.katra.backend.util.Instant2ISO;
import org.sdp8.katra.backend.util.LocalDate2ISO;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.Instant;
import java.time.LocalDate;
import java.util.*;

@Document(collection = "Reservations")
public class Reservation {
    @Id
    private String id;
    @JsonSerialize(converter = LocalDate2ISO.class)
    @JsonDeserialize(converter = ISO2LocalDate.class)
    private LocalDate checkInDate;
    @JsonSerialize(converter = LocalDate2ISO.class)
    @JsonDeserialize(converter = ISO2LocalDate.class)
    private LocalDate checkOutDate;
    private List<RoomReservationItem> rooms = new LinkedList<>();
    @JsonSerialize(converter = Instant2ISO.class)
    @JsonDeserialize(converter = ISO2Instant.class)
    private Instant timestamp = Instant.now();
    private String customerId;
    private List<String> remarks;

    private boolean seperateBills;

    private boolean pending;

    public String getId() {
        return id;
    }

    public LocalDate getCheckInDate() {
        return checkInDate;
    }

    public void setCheckInDate(LocalDate checkInDate) {
        this.checkInDate = checkInDate;
    }

    public LocalDate getCheckOutDate() {
        return checkOutDate;
    }

    public void setCheckOutDate(LocalDate checkOutDate) {
        this.checkOutDate = checkOutDate;
    }

    public List<RoomReservationItem> getRooms() {
        return rooms;
    }

    public void setRooms(List<RoomReservationItem> rooms) {
        this.rooms = rooms;
    }

    public Instant getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Instant timestamp) {
        this.timestamp = timestamp;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public List<String> getRemarks() {
        return remarks;
    }

    public void setRemarks(List<String> remarks) {
        this.remarks = remarks;
    }

    public boolean isPending() {
        return pending;
    }

    public void setPending(boolean pending) {
        this.pending = pending;
    }

    public boolean isSeperateBills() {
        return seperateBills;
    }

    public void setSeperateBills(boolean seperateBills) {
        this.seperateBills = seperateBills;
    }
}
