package org.sdp8.katra.backend.core.security

import org.sdp8.katra.backend.api.security.IUserManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.event.EventListener
import org.springframework.security.authentication.event.{AuthenticationSuccessEvent, AuthenticationFailureBadCredentialsEvent}
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.stereotype.Component

@Component
class AuthenticationEventHandler {
  @Autowired
  val userManager: IUserManager = null

  @EventListener
  def authSuccessEvent(e: AuthenticationSuccessEvent): Unit = {
    val username = Option(e.getAuthentication.getPrincipal match {
      case u: UserDetails => u.getUsername
      case s: String => s
      case _ => null
    })
    username.foreach(userManager.resetUserFailureCount)
  }

  @EventListener
  def authBadCredentialsEvent(e: AuthenticationFailureBadCredentialsEvent): Unit = {
    val username = Option(e.getAuthentication.getPrincipal match {
      case u: UserDetails => u.getUsername
      case s: String => s
      case _ => null
    })
    username.foreach(userManager.incrementUserFailureCount)
  }
}
