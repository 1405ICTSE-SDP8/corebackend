package org.sdp8.katra.modules.users

import org.springframework.context.annotation.{ComponentScan, Configuration}
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories

@Configuration
@ComponentScan
@EnableMongoRepositories(basePackages = Array("org.sdp8.katra.modules.users.repositories"))
class UsersConfiguration {

}
