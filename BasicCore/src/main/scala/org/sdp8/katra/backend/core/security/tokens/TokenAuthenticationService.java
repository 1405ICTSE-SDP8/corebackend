package org.sdp8.katra.backend.core.security.tokens;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.SignatureException;
import org.sdp8.katra.backend.api.security.IUserManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.CredentialsExpiredException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Optional;

@Component
public class TokenAuthenticationService {

    private static final String AUTH_HEADER_NAME = "Authorization";

    private final TokenHandler tokenHandler;
    private final IUserManager userManager;

    @Autowired
    public TokenAuthenticationService(TokenHandler tokenHandler, IUserManager userManager) {
        this.tokenHandler = tokenHandler;
        this.userManager = userManager;
    }

    public void addAuthentication(HttpServletResponse response, TokenAuthentication authentication) {
        final UserDetails user = authentication.getDetails();
        response.addHeader(AUTH_HEADER_NAME, tokenHandler.createToken(user, Optional.empty()));
    }

    public Authentication getAuthentication(HttpServletRequest request) {
        final String header = request.getHeader(AUTH_HEADER_NAME);
        if (header != null && header.startsWith(TokenHandler.authScheme)) {
            final String tokenString = header.substring(TokenHandler.authScheme.length()).trim();
            try {
                final Jws<Claims> token = tokenHandler.parseToken(tokenString);
                final UserDetails user = userManager.loadUserByUsername(token.getBody().getSubject());

                if (user != null) {
                    if (!user.isEnabled())
                        throw new DisabledException("User is disabled.");
                    return new TokenAuthentication(user);
                }
            }
            catch(ExpiredJwtException e) {
                throw new CredentialsExpiredException("Expired Token", e);
            }
            catch(SignatureException o0o) {
                throw new BadCredentialsException("Token Signature Invalid", o0o);
            }
        }
        return null;
    }
}