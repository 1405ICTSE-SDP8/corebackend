package org.sdp8.katra.modules.facilities.components

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.databind.{JsonSerializer, SerializerProvider}
import org.sdp8.katra.modules.facilities.entities.{FacilityAvailable, FacilityStatus, FacilityUnavailable}
import org.sdp8.katra.modules.rooms.entities.{RoomNotAvailable, RoomOccupied, RoomStatus}

class FacilityStatusJsonSerializer extends JsonSerializer[FacilityStatus] {
  override def serialize(value: FacilityStatus, gen: JsonGenerator, serializers: SerializerProvider): Unit = {
    gen.writeStartObject()
    gen.writeStringField("status", value.status)
    value match {
      case FacilityUnavailable(reason, extreason) =>
        gen.writeStringField("reason", reason)
        gen.writeStringField("extendedreason", extreason)
      case _ =>
    }
    gen.writeEndObject()
  }
}
