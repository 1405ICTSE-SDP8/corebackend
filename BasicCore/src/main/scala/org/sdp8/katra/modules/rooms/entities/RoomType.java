package org.sdp8.katra.modules.rooms.entities;

import com.fasterxml.jackson.annotation.JsonGetter;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Version;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document(collection = "RoomTypes")
public class RoomType {
    @Id
    String id;
    String name;
    Float baseRate;
    String description;
    List<String> facilities;
    @Version
    private long version;

    public long getVersion() {
        return version;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Float getBaseRate() {
        return baseRate;
    }

    public void setBaseRate(Float baseRate) {
        this.baseRate = baseRate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<String> getFacilities() {
        return facilities;
    }

    public void setFacilities(List<String> facilities) {
        this.facilities = facilities;
    }

    public RoomType(){}

    @JsonGetter
    private String getTypeId() {
        return this.getId();
    }
}
