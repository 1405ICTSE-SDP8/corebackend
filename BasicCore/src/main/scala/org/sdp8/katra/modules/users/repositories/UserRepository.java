package org.sdp8.katra.modules.users.repositories;

import org.sdp8.katra.modules.users.entities.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(path = "users")
public interface UserRepository extends MongoRepository<User, String> {
    User findByUsername(@Param("username") String username);
}
