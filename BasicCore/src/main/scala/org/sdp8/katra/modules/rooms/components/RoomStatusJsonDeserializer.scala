package org.sdp8.katra.modules.rooms.components

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.{DeserializationContext, JsonDeserializer, JsonNode, ObjectMapper}
import org.sdp8.katra.modules.rooms.entities._

class RoomStatusJsonDeserializer extends JsonDeserializer[RoomStatus] {
  override def deserialize(p: JsonParser, ctxt: DeserializationContext): RoomStatus = {
    val root: JsonNode = p.getCodec.readTree(p)
    val mapper = new ObjectMapper()

    val status = Option(root.get("status"))
    status foreach {
      _.asText().toLowerCase match {
        case RoomStatus.OCCUPIED => Option(root.get("occid"))
          .foreach {id => return RoomOccupied(id.asText())}
        case RoomStatus.NOT_AVAILABLE => Option(root.get("reason"))
          .foreach {reason =>
            return RoomNotAvailable(reason.asText(),
              root.path("extendedreason").asText())}
        case RoomStatus.AVAILABLE => return RoomAvailable
        case _ => return UnknownStatus
      }
    }
    throw new IllegalArgumentException("Invalid Room Status")
  }
}
