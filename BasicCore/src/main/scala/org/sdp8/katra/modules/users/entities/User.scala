package org.sdp8.katra.modules.users.entities

import java.security.SecureRandom
import java.time.Instant
import java.util
import java.util.Optional
import java.util.function.Consumer

import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility
import com.fasterxml.jackson.annotation._
import org.bson.types.ObjectId
import org.sdp8.katra.backend.api.security.IUser
import org.sdp8.katra.backend.core.security.SimpleGrantedAuthority
import org.sdp8.katra.modules.users.repositories.UserRepository
import org.sdp8.katra.modules.users.services.RoleService
import org.springframework.data.annotation.{Id, PersistenceConstructor, Version}
import org.springframework.data.mongodb.core.index.Indexed
import org.springframework.data.mongodb.core.mapping.Document
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.crypto.password.PasswordEncoder

//TODO: Accout status checks
@Document(collection = "Users")
@JsonAutoDetect(getterVisibility = Visibility.NONE,
  fieldVisibility = Visibility.NONE,
  setterVisibility = Visibility.NONE)
class User (@Indexed(unique = true) private val username: String) extends IUser {

  @PersistenceConstructor
  protected[users] def this() = {
    this(null.asInstanceOf[String])
  }

  def this(user: IUser) = {
    this(user.getUsername)
    this.password = user.getPassword
    this.permissions = user.getPermissions
    this.role = user.getRole
    this.enabled = user.isEnabled
  }

  @Id
  val id: ObjectId = null
  @JsonGetter
  override def getUID = id.toString

  private var password: String = null
  private var enabled: Boolean = false
  @JsonProperty
  private var role: String = null
  @JsonProperty
  private var permissions: util.List[String] = null

  private var failedCount: Int = 0
  @JsonProperty
  private var accountExpiry: Instant = null
  @JsonProperty
  private var passwordExpiry: Instant = null
  @JsonProperty
  private var customProperties: util.Map[String, String] = new util.HashMap[String, String]()
  @JsonIgnore
  private var renewalTokenKey: Array[Byte] = null

  @JsonGetter
  override def isEnabled: Boolean = enabled
  @JsonGetter
  override def getUsername: String = username
  override def getPassword: String = password
  @JsonGetter
  override def isAccountNonExpired: Boolean = if (accountExpiry == null) true
    else accountExpiry.isBefore(Instant.now())
  @JsonGetter
  override def isCredentialsNonExpired: Boolean = if (passwordExpiry == null) true
    else passwordExpiry.isBefore(Instant.now())

  /**
    * Gets a list of authorities.
    * All roles are processed into ROLE authorities.
    * and Permissions are processed into PERM authorities
    *
    * @return
    */
  override def getAuthorities: util.Collection[_ <: GrantedAuthority] = {
    val col = new util.LinkedList[GrantedAuthority]()
    col.add(new SimpleGrantedAuthority("ROLE_".concat(getRole)))
    //TODO: Role - perm expansion.
    val permProcessor: Consumer[String] = {
      case s: String if s.startsWith("-") =>
        col.removeIf(_.getAuthority == "PERM_".concat(s.substring(1)))
      case s: String => col.add(new SimpleGrantedAuthority("PERM_".concat(s)))
    }
    User.roleServ.getPermissionsForRole(this.getRole).foreach(_.stream().forEach(permProcessor))
    Option(this.permissions).foreach(_.stream().forEach(permProcessor))
    col
  }
  //TODO: Implement lockouts
  override def isAccountNonLocked: Boolean = {
    true
  }
  override def getRole: String = this.role
  override def removePermissions(permissions: String*): Unit = for (p <- permissions)
    this.permissions.removeIf(p2 => p == p2)
  override def getPermissions: util.List[String] = {
    if (permissions == null)
      new util.ArrayList[String]
    else
      permissions
  }
  override def setRole(role: String): Unit = this.role = role

  override def addPermissions(permissions: String*): Unit = {
    if (this.permissions == null)
      this.permissions = new util.ArrayList[String]

    for (p <- permissions) this.permissions.add(p)
  }
  @JsonSetter
  override def setEnabled(enabled: Boolean): Unit = this.enabled = enabled

  /**
    * Should null be passed as a password, this method will do nothing.
    */
  @JsonSetter
  override def setPassword(password: String): Unit = {
    Option(password).foreach(p => this.password = User.pwe.encode(p))
  }

  override def getAccountExpiry: Optional[Instant] = Optional.ofNullable(accountExpiry)

  override def setAccountExpiry(instant: Instant): Unit = accountExpiry = instant

  override def getPasswordExpiry: Optional[Instant] = Optional.ofNullable(passwordExpiry)

  override def setPasswordExpiry(instant: Instant): Unit = passwordExpiry = instant

  override def getFailureCount: Int = failedCount

  override def setFailureCount(count: Int): Unit = failedCount = count

  @Version
  private var version: Long = 0

  override def getCustomProperties: util.Map[String, String] = customProperties

  override def setCustomProperties(props: util.Map[String, String]): Unit = customProperties = props

  override def getRenewalTokenKey: Array[Byte] = {
    val tokenKey = Option(renewalTokenKey)
    if (tokenKey.isEmpty)
      resetRenewalTokenKey()
    renewalTokenKey
  }

  override def resetRenewalTokenKey(): Unit = {
    val bytes = new Array[Byte](16)
    SecureRandom.getInstanceStrong.nextBytes(bytes)
    renewalTokenKey = bytes
    Option(id).foreach(id => User.repo.save(this))
  }
}
object User {
  var pwe: PasswordEncoder = null
  var repo: UserRepository = null
  var roleServ: RoleService = null
}