package org.sdp8.katra.modules.billing.repositories;

import org.sdp8.katra.modules.billing.entities.Payment;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

public interface PaymentRepository extends MongoRepository<Payment, String> {

    @Query("{'invoices':{'$in':[?0]}}")
    Payment findByInvoiceId(String invoiceId);
}
