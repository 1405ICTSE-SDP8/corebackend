package org.sdp8.katra.modules.rooms.repositories;

import org.bson.types.ObjectId;
import org.sdp8.katra.modules.rooms.entities.Room;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

@RepositoryRestResource(path = "rooms")
public interface RoomRepository extends MongoRepository<Room, String>{

    @Query("{'$and':[{'prefix':?0},{'id':?1}]}")
    Room findRoomByPrefixAndId(@Param("prefix") String prefix, @Param("id") String id);

    List<Room> findAllByRoomTypeId(@Param("roomTypeId") String roomTypeId);

    @Query("{'roomStatus.status':?0}")
    List<Room> findAllByRoomStatus(@Param("status") String status);

    @Query("{'roomStatus.status':?0, 'roomTypeId': ?1}")
    List<Room> findAllByRoomStatusAndTypeId(@Param("status") String status, @Param("roomTypeId") String roomTypeId);

    @Query("{'roomStatus.status':?0, 'roomTypeId': ?1, 'prefix': ?2}")
    Room findByPrefixRoomStatusAndTypeId(@Param("status") String status,
                                         @Param("roomTypeId") String roomTypeId,
                                         @Param("prefix") String prefix);

    @Query("{'$and':[{'roomStatus.status':?0}, {'roomTypeId': ?1}, {'prefix': {'$in': ?2}}]}")
    Room findByPrefixesRoomStatusAndTypeId(@Param("status") String status,
                                         @Param("roomTypeId") String roomTypeId,
                                         @Param("prefixes") List<String> prefixes);

    @Query("{'$and':[{'roomStatus.status':?0}, {'roomTypeId': ?1}, {'prefix': {'$in': ?2}}, {'_id':{'$nin': ?3}}]}")
    Room findByPrefixesRoomStatusTypeIdExcept(@Param("status") String status,
                                              @Param("roomTypeId") String roomTypeId,
                                              @Param("prefixes") List<String> prefixes,
                                              @Param("exceptIds") List<ObjectId> exceptIds);

    @Query("{'roomStatus.status':?0, 'roomTypeId': ?1}")
    Room findByRoomStatusAndTypeId(@Param("status") String status, @Param("roomTypeId") String roomTypeId);
    @Query("{'$and':[{'roomStatus.status':?0}, {'roomTypeId': ?1}, {'_id':{'$nin': ?3}}]}")
    Room findByRoomStatusAndTypeIdExcept(@Param("status") String status,
                                         @Param("roomTypeId") String roomTypeId,
                                         @Param("exceptIds") List<ObjectId> exceptIds);

    @Query("{'$and':[{'roomTypeId':?0},{'roomStatus.status':?1}]}")
    long countRoomByTypeAndStatus(@Param("typeId") String typeId, @Param("status") String status);

    @Query("{'$and':[{'roomTypeId':?0},{'enabled':?1}]}")
    long countEnabledByType(String typeId, boolean enabled);
}
