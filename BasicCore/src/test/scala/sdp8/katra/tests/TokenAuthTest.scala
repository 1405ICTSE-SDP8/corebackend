package sdp8.katra.tests

import java.time.Instant
import java.time.temporal.ChronoUnit
import java.util.Optional

import org.junit.Test
import org.junit.runner.RunWith
import org.sdp8.katra.backend.Application
import org.sdp8.katra.backend.core.security.tokens.TokenHandler
import org.sdp8.katra.modules.datastore.hybrid.DBConfiguration
import org.sdp8.katra.modules.users.UsersConfiguration
import org.sdp8.katra.modules.users.entities.User
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.core.env.Environment
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner

@RunWith(classOf[SpringJUnit4ClassRunner])
@ContextConfiguration(classes = Array(classOf[Application],
  classOf[DBConfiguration],
  classOf[UsersConfiguration]))
class TokenAuthTest {
  @Autowired
  val tokenH: TokenHandler = null
  @Autowired
  val env: Environment = null

  @Test
  def testTokenGeneration(): Unit = {
    val user = new User("tokenTest")
    user.setEnabled(true)
    user.setRole("TEST")
    user.addPermissions("katra.test")
    System.out.println(tokenH.createToken(user, Optional.of(Instant.now.plus(30, ChronoUnit.MINUTES))))
  }
}
