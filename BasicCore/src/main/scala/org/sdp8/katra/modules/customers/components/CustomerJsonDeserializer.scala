package org.sdp8.katra.modules.customers.components

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind._
import com.fasterxml.jackson.databind.node.ObjectNode
import org.sdp8.katra.modules.customers.entities.{Customer, Organization, Person}


class CustomerJsonDeserializer extends JsonDeserializer[Customer]{
  override def deserialize(p: JsonParser, ctxt: DeserializationContext): Customer = {
    val orgFlag = "_organizationFlag"
    val codec = p.getCodec
    val root: ObjectNode = codec.readTree(p)
    val flag = root.path(orgFlag)
    if (flag.isMissingNode)
      throw new RuntimeJsonMappingException(s"$orgFlag not present, unable to determine type.")
    root.remove(orgFlag)
    if (flag.asBoolean())
      codec.treeToValue(root, classOf[Organization])
    else
      codec.treeToValue(root, classOf[Person])
  }
}
