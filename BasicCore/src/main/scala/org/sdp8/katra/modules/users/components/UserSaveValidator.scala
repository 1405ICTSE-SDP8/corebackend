package org.sdp8.katra.modules.users.components

import org.apache.log4j.Logger
import org.springframework.validation.{Errors, Validator}
import org.sdp8.katra.modules.users.entities.User
import org.sdp8.katra.modules.users.repositories.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

class UserSaveValidator extends Validator {
  @Autowired
  val repo: UserRepository = null

  override def validate(target: scala.Any, errors: Errors): Unit = {
    Logger.getLogger("DEBUG").debug("Validating User")

  }

  override def supports(clazz: Class[_]): Boolean = classOf[User].isAssignableFrom(clazz)
}
