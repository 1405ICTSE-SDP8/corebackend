package org.sdp8.katra.backend.api.security;

import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.Optional;

public interface IUserManager extends UserDetailsService{
    /**
     * Add a new user, password is seperate
     * to allow user manager to perform password encoding.
     * @param user User to add
     * @param password Password of the user
     */
    void addUser(IUser user, String password);
    Optional<? extends IUser> getUser(String username);
    void removeUser(String username);
    void saveUser(IUser user);

    void setPassword(String username, String password);

    void incrementUserFailureCount(String username);
    void resetUserFailureCount(String username);

    long countUsers();

    UserBuilder getUserBuilder();
}
