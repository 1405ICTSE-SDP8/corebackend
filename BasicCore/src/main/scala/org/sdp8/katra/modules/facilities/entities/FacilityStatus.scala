package org.sdp8.katra.modules.facilities.entities

sealed abstract class FacilityStatus(val status: String)
case object UnknownStatus extends FacilityStatus(FacilityStatus.UNKNOWN)
case object FacilityAvailable extends FacilityStatus(FacilityStatus.AVAILABLE)
case class FacilityUnavailable(reason: String, extendedReason: String) extends FacilityStatus(FacilityStatus.NOT_AVAILABLE)
object FacilityStatus {
  val AVAILABLE = "available"
  val NOT_AVAILABLE = "unavailable"
  val UNKNOWN = "unknown"
}