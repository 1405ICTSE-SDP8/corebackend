package org.sdp8.katra.modules.bookings.validation

import java.time.ZoneOffset

import org.apache.log4j.Logger
import org.sdp8.katra.modules.bookings.entities.Reservation
import org.sdp8.katra.modules.bookings.repositories.ReservationRepository
import org.sdp8.katra.modules.rooms.repositories.RoomRepository
import org.springframework.validation.{Errors, Validator}

import scala.collection.JavaConverters._

class ReservationDateRoomValidator(rsvps: ReservationRepository, rooms: RoomRepository) extends Validator {
  val logger = Logger.getLogger(classOf[ReservationDateRoomValidator])

  override def validate(target: scala.Any, errors: Errors): Unit = {
    val obj = target.asInstanceOf[Reservation]
    if (obj.getCheckInDate.isAfter(obj.getCheckOutDate))
      errors.reject("InvalidDates", "Check in date cannot be after check out date!")

    if (obj.getRooms.isEmpty)
      errors.rejectValue("rooms", "NoRoomsSelected", "No rooms are going to be reserved")

    val rsvpRooms: Map[String, Short] = (for (i <- obj.getRooms.asScala)
      yield {i.getRoomTypeId -> i.getAmount})(collection.breakOut)

    obj.getRooms.forEach(i => {
      if (rooms.countEnabledByType(i.getRoomTypeId, true) <= 0)
        errors.rejectValue("rooms", "RoomNotAvailable", s"No free rooms of type ${i.getRoomTypeId}")
    })

    rsvps.findAllOverlapping(obj.getCheckInDate.atStartOfDay(ZoneOffset.UTC).toInstant,
      obj.getCheckOutDate.atTime(23, 59, 59).toInstant(ZoneOffset.UTC)).asScala.iterator
      .withFilter(_.getId != obj.getId)
      .map{_.getRooms.asScala.withFilter {k2 => rsvpRooms.contains(k2.getRoomTypeId)} }
      .foldLeft(new scala.collection.mutable.HashMap[String, Int]()) { case (acc, m) =>
        m.foreach { r => acc.put(r.getRoomTypeId, acc.getOrElse(r.getRoomTypeId, 0) + r.getAmount) }
        acc
      }
      .withFilter { case (k, v) => rooms.countEnabledByType(k, true) <= v + rsvpRooms(k)}
      .foreach { case (k, v) => errors.rejectValue("rooms", "RoomNotAvailable", s"No free rooms of type $k") }
  }

  override def supports(clazz: Class[_]): Boolean = classOf[Reservation].isAssignableFrom(clazz)
}
