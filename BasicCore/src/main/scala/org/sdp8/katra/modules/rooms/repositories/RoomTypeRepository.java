package org.sdp8.katra.modules.rooms.repositories;

import org.sdp8.katra.modules.rooms.entities.RoomType;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(path = "roomTypes")
public interface RoomTypeRepository extends MongoRepository<RoomType, String> {}
