package org.sdp8.katra.modules.customers.repositories;

import org.sdp8.katra.modules.customers.CustomerIdentity;
import org.sdp8.katra.modules.customers.entities.Customer;
import org.sdp8.katra.modules.customers.entities.Person;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

@RepositoryRestResource(path = "customers")
public interface CustomerRepository extends MongoRepository<Customer, String> {
    Person findByIdentifier(@Param("ident") CustomerIdentity identifier);
    Person findByName(@Param("name") String string);

    @Query("{'name':{'$regex':?0, '$option':'i'}}")
    Iterable<Person> findAllByNameRegex(@Param("pattern") String pattern);

    @Query("{'name':{'$regex':?0, '$option':?1}}")
    @RestResource(path="regex")
    Iterable<Person> findAllByNameRegex(@Param("pattern") String pattern, @Param("option") String option);
}
