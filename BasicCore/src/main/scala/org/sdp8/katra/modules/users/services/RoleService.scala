package org.sdp8.katra.modules.users.services

import java.io.{File, FileOutputStream}
import java.nio.charset.Charset
import java.util
import javax.annotation.PostConstruct

import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.core.env.Environment
import org.springframework.stereotype.Service


@Service
class RoleService {

  private type roleList = util.Map[String,util.List[String]]
  private var roles: roleList = null

  private val mapper: ObjectMapper = new ObjectMapper()

  @Autowired
  private val env: Environment = null

  @PostConstruct
  def loadFromFile(): Unit = {
    val rolesFile = env.getProperty("katra.roles.file", "roles.json")
    val file = new File(rolesFile)

    if (file.exists())
      roles = mapper.readValue(file, classOf[roleList])
    else {
      val out = new FileOutputStream(file)
      out.write(Charset.forName("UTF-8").encode("{}").array())
      out.close()
      roles = new util.HashMap[String, util.List[String]]()
    }
  }

  def getRoles: util.Set[String] = roles.keySet()

  def getPermissionsForRole(role: String): Option[util.List[String]] = Option(role).flatMap(s => Option(roles.get(s)))
}
