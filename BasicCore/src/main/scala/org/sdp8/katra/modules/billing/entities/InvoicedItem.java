package org.sdp8.katra.modules.billing.entities;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.annotation.PersistenceConstructor;

public class InvoicedItem {

    @PersistenceConstructor
    private InvoicedItem(){}

    @JsonCreator
    public InvoicedItem(@JsonProperty("item")String item,
                        @JsonProperty("unitPrice") double unitPrice,
                        @JsonProperty("amount") int amount) {
        this.item = item;
        this.unitPrice = unitPrice;
        this.amount = amount;
    }

    private String item;
    private double unitPrice;
    private int amount;

    public String getItem() {
        return item;
    }

    public double getUnitPrice() {
        return unitPrice;
    }

    public int getAmount() {
        return amount;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public void setUnitPrice(double unitPrice) {
        this.unitPrice = unitPrice;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
}
