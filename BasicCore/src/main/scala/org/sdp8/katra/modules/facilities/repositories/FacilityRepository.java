package org.sdp8.katra.modules.facilities.repositories;

import org.sdp8.katra.modules.facilities.entities.Facility;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(path = "facilities")
public interface FacilityRepository extends MongoRepository<Facility, String> {
}
