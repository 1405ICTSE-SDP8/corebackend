package org.sdp8.katra.backend.core.rest

import java.time.format.DateTimeParseException

import org.springframework.data.repository.support.QueryMethodParameterConversionException
import org.springframework.data.rest.webmvc.RepositoryRestExceptionHandler
import org.springframework.http.{HttpHeaders, HttpStatus, ResponseEntity}
import org.springframework.web.bind.annotation.{ControllerAdvice, ExceptionHandler}

@ControllerAdvice
class RESTExceptionHandler {

  @ExceptionHandler(value = Array(classOf[QueryMethodParameterConversionException]))
  def handleDateTimeParsingError(e: Throwable): ResponseEntity[_] = {
    e match {
      case qme: QueryMethodParameterConversionException =>
        ResponseEntity.badRequest().body(qme.getMessage)
    }
  }
}