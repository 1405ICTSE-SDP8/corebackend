package org.sdp8.katra.modules.billing.repositories;

import org.sdp8.katra.modules.billing.entities.Invoice;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.security.access.prepost.PreAuthorize;

import java.util.List;

public interface InvoiceRepository extends MongoRepository<Invoice, String> {

    @PreAuthorize("hasAuthority(PERM_katra.invoice.directUpdate)")
    <S extends Invoice> List<S> save(Iterable<S> entites);

    List<Invoice> findAllByReservationId(String rsvpId);
    List<Invoice> findAllByCustomerId(String customerId);
    Invoice findByStayId(String stayId);
}
