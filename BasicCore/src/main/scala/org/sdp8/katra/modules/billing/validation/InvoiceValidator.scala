package org.sdp8.katra.modules.billing.validation

import org.sdp8.katra.modules.billing.entities.Invoice
import org.sdp8.katra.modules.customers.repositories.CustomerRepository
import org.springframework.validation.{Errors, Validator}

class InvoiceValidator(val custs: CustomerRepository) extends Validator {

  override def validate(target: scala.Any, errors: Errors): Unit = {
    val obj = target.asInstanceOf[Invoice]
    if(obj.getCustomerId == null || obj.getCustomerId.isEmpty || custs.findOne(obj.getCustomerId) == null)
      errors.rejectValue("customerId","InvalidCustomer","Invalid Customer ID")
    if (obj.getItems.isEmpty)
      errors.rejectValue("items","EmptyInvoice","Invoice has no items.")
  }

  override def supports(clazz: Class[_]): Boolean = classOf[Invoice].isAssignableFrom(clazz)
}
