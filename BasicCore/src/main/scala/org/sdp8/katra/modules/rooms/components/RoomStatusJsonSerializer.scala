package org.sdp8.katra.modules.rooms.components

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.databind.{JsonSerializer, SerializerProvider}
import org.sdp8.katra.modules.rooms.entities.{RoomAvailable, RoomNotAvailable, RoomOccupied, RoomStatus}

class RoomStatusJsonSerializer extends JsonSerializer[RoomStatus] {
  override def serialize(value: RoomStatus, gen: JsonGenerator, serializers: SerializerProvider): Unit = {
    gen.writeStartObject()
    gen.writeStringField("status", value.status)
    value match {
      case RoomOccupied(oid) =>
        gen.writeStringField("occid", oid)
        gen
      case RoomNotAvailable(reason, extreason) =>
        gen.writeStringField("reason", reason)
        gen.writeStringField("extendedreason", extreason)
      case _ =>
    }
    gen.writeEndObject()
  }
}
