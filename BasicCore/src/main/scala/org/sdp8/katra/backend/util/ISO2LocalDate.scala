package org.sdp8.katra.backend.util

import java.time.{Instant, LocalDate}

import com.fasterxml.jackson.databind.util.Converter
import com.fasterxml.jackson.databind.{DeserializationContext, JavaType, JsonDeserializer}
import com.fasterxml.jackson.databind.`type`.TypeFactory

import scala.reflect.ClassTag

abstract class SimpleJSONStringConverter[OUT](val cl: Class[OUT]) extends Converter[String, OUT] {
  override def getOutputType(typeFactory: TypeFactory): JavaType =
    typeFactory.constructSimpleType(cl, cl, Array())

  override def getInputType(typeFactory: TypeFactory): JavaType =
    typeFactory.constructSimpleType(classOf[String], classOf[String], Array())
}

abstract class SimpleJSONStringSerializer[IN](val cl: Class[IN]) extends Converter[IN, String] {

  override def getOutputType(typeFactory: TypeFactory): JavaType =
    typeFactory.constructSimpleType(classOf[String], classOf[String], Array())

  override def getInputType(typeFactory: TypeFactory): JavaType =
    typeFactory.constructSimpleType(cl, cl, Array())
}

class ISO2LocalDate extends SimpleJSONStringConverter(classOf[LocalDate]) {
  override def convert(value: String): LocalDate = LocalDate.parse(value)
}

class LocalDate2ISO extends SimpleJSONStringSerializer(classOf[LocalDate]) {
  override def convert(value: LocalDate): String = value.toString
}

class ISO2Instant extends SimpleJSONStringConverter(classOf[Instant]) {
  override def convert(value: String): Instant = Instant.parse(value)
}

class Instant2ISO extends SimpleJSONStringSerializer(classOf[Instant]) {
  override def convert(value: Instant): String = value.toString
}
