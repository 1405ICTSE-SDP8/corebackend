package org.sdp8.katra.backend.util

import java.util.Optional

import org.springframework.util.MultiValueMap

import scala.collection.JavaConversions._

object ImplicitConversions {
  implicit def springMVMapToScalaMap[K,V](value: MultiValueMap[K, V]): Map[K, Seq[V]] =
    value.toMap.mapValues(_.toIndexedSeq)

  implicit def optionalToOption[T](value: Optional[T]): Option[T] =
    if (value.isPresent)
      Option(value.get())
    else
      None

  implicit class Java8Option[T](val value: Option[T]) {
    def toJava: Optional[T] =
      if (value.isDefined)
        Optional.of(value.get)
      else
        Optional.empty()
  }
}
