package org.sdp8.katra.modules.billing.entities;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Version;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document(collection = "Payments")
public class Payment {
    @Id
    private String id;

    /**
     * Invoices paid for
     */
    private List<String> invoices;

    /**
     * This property will be null if no transaction id is applicable.
     */
    private String transId;
    /**
     * Method used for payment: CASH, MASTERCARD, PAYPAL , etc.
     */
    private String method;
    /**
     * Anmount paid
     */
    private double amount;

    public String getPaymentId() {
        return id;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getTransId() {
        return transId;
    }

    public void setTransId(String transId) {
        this.transId = transId;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public List<String> getInvoices() {
        return invoices;
    }

    public void setInvoices(List<String> invoices) {
        this.invoices = invoices;
    }

    @Version
    private long version;

    public long getVersion() {
        return version;
    }
}

