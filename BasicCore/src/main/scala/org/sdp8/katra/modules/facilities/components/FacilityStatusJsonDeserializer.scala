package org.sdp8.katra.modules.facilities.components

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.{DeserializationContext, JsonDeserializer, JsonNode, ObjectMapper}
import org.sdp8.katra.modules.facilities.entities.{FacilityAvailable, FacilityStatus, FacilityUnavailable, UnknownStatus}

class FacilityStatusJsonDeserializer extends JsonDeserializer[FacilityStatus] {
  override def deserialize(p: JsonParser, ctxt: DeserializationContext): FacilityStatus = {
    val root: JsonNode = p.getCodec.readTree(p)
    val mapper = new ObjectMapper()

    val status = Option(root.get("status"))
    status foreach {
      _.asText().toLowerCase match {
        case FacilityStatus.NOT_AVAILABLE => Option(root.get("reason"))
          .foreach {reason =>
            return FacilityUnavailable(reason.asText(),
              root.path("extendedreason").asText())}
        case FacilityStatus.AVAILABLE => return FacilityAvailable
        case _ => return UnknownStatus
      }
    }
    throw new IllegalArgumentException("Invalid Facility Status")
  }
}
