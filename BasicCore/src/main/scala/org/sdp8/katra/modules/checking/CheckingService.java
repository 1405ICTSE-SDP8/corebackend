package org.sdp8.katra.modules.checking;

import org.bson.types.ObjectId;
import org.sdp8.katra.modules.billing.entities.Invoice;
import org.sdp8.katra.modules.billing.entities.Payment;
import org.sdp8.katra.modules.billing.repositories.InvoiceRepository;
import org.sdp8.katra.modules.billing.repositories.PaymentRepository;
import org.sdp8.katra.modules.bookings.entities.Reservation;
import org.sdp8.katra.modules.bookings.entities.RoomReservationItem;
import org.sdp8.katra.modules.bookings.entities.Stay;
import org.sdp8.katra.modules.bookings.repositories.StayRepository;
import org.sdp8.katra.modules.customers.entities.Customer;
import org.sdp8.katra.modules.rooms.entities.*;
import org.sdp8.katra.modules.rooms.repositories.RoomRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.time.ZoneOffset;
import java.time.temporal.TemporalAmount;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class CheckingService {
    @Autowired
    StayRepository stayRepo;
    @Autowired
    RoomRepository roomRepo;
    @Autowired
    InvoiceRepository invRepo;
    @Autowired
    PaymentRepository payRepo;


    public List<Stay> checkIn(Reservation rsvp) {
        if (rsvp.isSeperateBills())
            throw new IllegalArgumentException("Wrong function for multibill reservations,");
        Set<String> assignedPrefixes = new HashSet<>();
        List<ObjectId> roomIds = new ArrayList<>(rsvp.getRooms().size());
        List<Stay> stays = rsvp.getRooms().stream().map( r -> {
            Stay stay = new Stay();
            stay.setCustomerId(rsvp.getCustomerId());
            //Todo: Fix Time Zone
            stay.setExpectedEndTime(rsvp.getCheckOutDate().atTime(3,0).toInstant(ZoneOffset.UTC));
            //Todo: Confirm if stored rate is hourly or daily
            stay.setRate(r.getRate());
            stay.setReservationId(rsvp.getId());
            stay.setStartTime(Instant.now());
            Room room = roomRepo.findByPrefixesRoomStatusTypeIdExcept(RoomStatus.AVAILABLE(),
                    r.getRoomTypeId(),
                    new ArrayList<>(assignedPrefixes),
                    roomIds);
            if (room == null)
                room = roomRepo.findByRoomStatusAndTypeIdExcept(RoomStatus.AVAILABLE(), r.getRoomTypeId(), roomIds);
            if (room != null) {
                stay.setRoomId(room.getId());
                roomIds.add(new ObjectId(room.getId()));
                assignedPrefixes.add(room.getPrefix());
                return stay;
            }
            else return null;
        }).filter(s -> s != null).collect(Collectors.toList());
        stays = stayRepo.save(stays);
        //Flag rooms as occupied.
        stays.forEach(s -> {
            Room room = roomRepo.findOne(s.getRoomId());
            room.setRoomStatus(new RoomOccupied(s.getId()));
            roomRepo.save(room);
        });
        return stays;
    }

    public List<Stay> checkIn(Reservation rsvp, Map<RoomReservationItem, Customer> customers) {
        //Verify customer map is complete
        if (customers.keySet().containsAll(rsvp.getRooms()))
            throw new IllegalArgumentException("Customer map invalid missing entries!");
        Set<String> assignedPrefixes = new HashSet<>();
        List<ObjectId> roomIds = new ArrayList<>(customers.size());
        List<Stay> stays = customers.entrySet().stream().map(e -> {
            RoomReservationItem r = e.getKey();
            Customer c = e.getValue();
            Stay s = new Stay();
            s.setCustomerId(c.getId());
            s.setStartTime(Instant.now());
            //Todo: Fix Time Zone
            s.setExpectedEndTime(rsvp.getCheckOutDate().atTime(3,0).toInstant(ZoneOffset.UTC));
            s.setRate(r.getRate());
            s.setReservationId(rsvp.getId());
            Room room = roomRepo.findByPrefixesRoomStatusTypeIdExcept(RoomStatus.AVAILABLE(),
                    r.getRoomTypeId(),
                    new ArrayList<>(assignedPrefixes),
                    roomIds);
            if (room == null)
                room = roomRepo.findByRoomStatusAndTypeIdExcept(RoomStatus.AVAILABLE(), r.getRoomTypeId(), roomIds);
            if (room != null) {
                s.setRoomId(room.getId());
                roomIds.add(new ObjectId(room.getId()));
                assignedPrefixes.add(room.getPrefix());
                return s;
            }
            else return null;
        }).filter(s -> s != null).collect(Collectors.toList());

        stays = stayRepo.save(stays);
        //Flag rooms as occupied.
        stays.forEach(s -> {
            Room room = roomRepo.findOne(s.getRoomId());
            room.setRoomStatus(new RoomOccupied(s.getId()));
            roomRepo.save(room);
        });
        return stays;
    }

    public Stay checkIn(Customer cust, TemporalAmount duration, RoomType roomType) {
        Stay stay = new Stay();
        stay.setCustomerId(cust.getId());
        stay.setStartTime(Instant.now());
        stay.setRate(roomType.getBaseRate());
        stay.setReservationId(null);
        stay.setExpectedEndTime(stay.getStartTime().plus(duration));
        Room room = roomRepo.findByRoomStatusAndTypeId(RoomStatus.AVAILABLE(), roomType.getId());
        stay.setRoomId(room.getId());
        stay = stayRepo.save(stay);
        //Flag room as occupied.
        room.setRoomStatus(new RoomOccupied(stay.getId()));
        roomRepo.save(room);
        return stay;
    }

    /**
     * Checkout for entire reservation.
     * @param rsvp
     */
    public void checkOut(Reservation rsvp) {
        if (rsvp.isSeperateBills()) {
            //Verify all invoices are paid
            List<String> invoicesPaid = new LinkedList<>();
            List<Invoice> invoices = invRepo.findAllByReservationId(rsvp.getId());
            if (invoices.isEmpty())
                throw new IllegalArgumentException("Invoices has not been generated!");
            invoices.stream().forEach(i -> {
                if (!invoicesPaid.contains(i.getId())) {
                    Payment pay = payRepo.findByInvoiceId(i.getId());
                    if (pay == null)
                        throw new IllegalArgumentException("All invoices has not been settled!");
                    else
                        invoicesPaid.addAll(pay.getInvoices());
                }
            });

            //Unflag all occupied rooms.
            Iterable<Stay> stays = stayRepo.findAllByReservationId(rsvp.getId());
            stays.forEach(s -> {
                Room room = roomRepo.findOne(s.getRoomId());
                room.setRoomStatus(RoomAvailable$.MODULE$);
                roomRepo.save(room);
            });
        }
    }

    public void checkOut(Stay stay) {
        Invoice inv = invRepo.findByStayId(stay.getId());
        if (inv == null)
            throw new IllegalArgumentException("Invoice has not been generated");
        Payment pay = payRepo.findByInvoiceId(inv.getId());
        if (pay == null)
            throw new IllegalArgumentException("Invoice has not been settled.");

        Room room = roomRepo.findOne(stay.getRoomId());
        room.setRoomStatus(RoomAvailable$.MODULE$);
        roomRepo.save(room);
    }
}
