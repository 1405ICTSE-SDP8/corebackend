package org.sdp8.katra.modules.bookings.repositories;

import org.sdp8.katra.modules.bookings.entities.Stay;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(path = "stays")
public interface StayRepository extends MongoRepository<Stay, String> {

    Iterable<Stay> findAllByReservationId(String id);
}
