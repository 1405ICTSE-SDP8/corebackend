package org.sdp8.katra.modules.bookings;

import org.sdp8.katra.modules.bookings.repositories.ReservationRepository;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@Configuration
@ComponentScan
@EnableMongoRepositories(basePackageClasses = {ReservationRepository.class})
public class BookingConfiguration {
}
