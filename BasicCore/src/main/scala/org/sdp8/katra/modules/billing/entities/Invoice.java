package org.sdp8.katra.modules.billing.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Version;

import java.util.LinkedList;
import java.util.List;

@JsonIgnoreProperties(value = {"subtotal", "total", "invoiceId"}, allowGetters = true)
public class Invoice {

    @Id
    private String id;
    private String customerId;
    private String reservationId;
    private String stayId;
    private List<InvoicedItem> items = new LinkedList<>();
    private List<ExtraCharge> extraCharges = new LinkedList<>();

    @JsonProperty("invoiceId")
    public String getId() {
        return id;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public List<InvoicedItem> getItems() {
        return items;
    }

    public void setItems(List<InvoicedItem> items) {
        this.items = items;
    }

    public List<ExtraCharge> getExtraCharges() {
        return extraCharges;
    }

    public void setExtraCharges(List<ExtraCharge> extraCharges) {
        this.extraCharges = extraCharges;
    }

    public String getStayId() {
        return stayId;
    }

    public void setStayId(String stayId) {
        this.stayId = stayId;
    }

    public String getReservationId() {
        return reservationId;
    }

    public void setReservationId(String reservationId) {
        this.reservationId = reservationId;
    }

    @JsonProperty("subtotal")
    public double getSubtotal() {
        return this.items.stream()
                .mapToDouble(i -> i.getUnitPrice() * i.getAmount())
                .sum();
    }

    @JsonProperty("total")
    public double getTotal() {
        double subtotal = getSubtotal();
        double charges = extraCharges.stream()
                .mapToDouble(i -> {
                    if (i.getType() == ExtraCharge.ChargeType.ABSOLUTE)
                        return i.getModifier();
                    else if (i.getType() == ExtraCharge.ChargeType.FRACTIONAL)
                        return subtotal * i.getModifier();
                    else return 0D;
                })
                .sum();
        return subtotal + charges;
    }

    @Version
    private long version;

    public long getVersion() {
        return version;
    }
}