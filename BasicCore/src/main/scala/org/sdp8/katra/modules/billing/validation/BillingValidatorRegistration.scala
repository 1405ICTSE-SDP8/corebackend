package org.sdp8.katra.modules.billing.validation

import org.sdp8.katra.backend.api.ValidatorRegistrationAgent
import org.sdp8.katra.modules.bookings.repositories.ReservationRepository
import org.sdp8.katra.modules.customers.repositories.CustomerRepository
import org.sdp8.katra.modules.rooms.repositories.RoomRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.rest.core.event.ValidatingRepositoryEventListener
import org.springframework.stereotype.Component

@Component
class BillingValidatorRegistration extends ValidatorRegistrationAgent{
  @Autowired
  val repo: CustomerRepository = null

  override def registerValidators(v: ValidatingRepositoryEventListener): Unit = {
    val validator = new InvoiceValidator(repo)
    v.addValidator("beforeCreate", validator)
    v.addValidator("beforeSave", validator)
  }
}
