package org.sdp8.katra.modules.customers.entities;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.sdp8.katra.modules.customers.CustomerIdentity;
import org.sdp8.katra.modules.customers.components.CustomerJsonDeserializer;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Version;
import org.springframework.data.mongodb.core.mapping.Document;


@Document(collection = "Customers")
@JsonDeserialize(using = CustomerJsonDeserializer.class)
public abstract class Customer {
    @JsonProperty(value = "_organizationFlag")
    abstract public boolean isOrganization();

    @Id
    private String id;

    private String name;

    private String email;

    private String address;

    private String contactNumber;

    private CustomerIdentity identifier;

    @Version
    private Long version;

    public String getId() {
        return id;
    }

    public Long getVersion() {
        return version;
    }

    public CustomerIdentity getIdentifier() {
        return identifier;
    }

    public void setIdentifier(CustomerIdentity identifier) {
        this.identifier = identifier;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @JsonGetter
    private String getCustomerId() {
        return this.getId();
    }
}
