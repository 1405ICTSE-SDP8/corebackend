package org.sdp8.katra.modules.users.components

import org.apache.log4j.Logger
import org.sdp8.katra.backend.api.ValidatorRegistrationAgent
import org.springframework.data.rest.core.event.ValidatingRepositoryEventListener
import org.springframework.stereotype.Component

@Component("UserValidatorRegistration")
class UserValidatorRegistration extends ValidatorRegistrationAgent{
  override def registerValidators(v: ValidatingRepositoryEventListener): Unit = {
    Logger.getLogger(classOf[UserValidatorRegistration]).debug("Registering Validators")
    v.addValidator("beforeSave", new UserSaveValidator)
  }
}
