package org.sdp8.katra.backend.core.rest;

import org.sdp8.katra.backend.core.rest.endpoints.Authentication;
import org.springframework.data.rest.webmvc.RepositoryLinksResource;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.LinkBuilder;
import org.springframework.hateoas.ResourceProcessor;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.stereotype.Component;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@Component
public class RestResourceProcessor implements ResourceProcessor<RepositoryLinksResource> {

    @Override
    public RepositoryLinksResource process(RepositoryLinksResource resource) {
        Class<Authentication> aclz = Authentication.class;
        Link loginLink = ControllerLinkBuilder.linkTo(aclz, methodOn(aclz).login("", "")).withRel("login");
        resource.add(loginLink);
        return resource;
    }
}