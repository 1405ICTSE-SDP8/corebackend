package org.sdp8.katra.modules.billing.components

import java.util

import org.sdp8.katra.modules.bookings.repositories.ReservationRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.rest.webmvc.{PersistentEntityResource, PersistentEntityResourceAssembler, RepositoryRestController}
import org.springframework.web.bind.annotation.RequestMapping

import scala.collection.JavaConverters._
import java.util.{List => JList}

@RepositoryRestController
class BillingController {
  @Autowired
  val billingServ: BillingService = null
  @Autowired
  val rsvpRepo: ReservationRepository = null

  @RequestMapping(path = Array())
  def generateInvoiceForReservation(resId: String, res: PersistentEntityResourceAssembler): JList[PersistentEntityResource] = {
    val rsvp = Option(resId).flatMap(s => Option(rsvpRepo.findOne(s)))
    val invoices = rsvp.map(billingServ.generateInvoicesFromReservation(_, dryRun = false))

    invoices.map(i => i.asScala.map(res.toResource).toList.asJava)
      .getOrElse(new util.ArrayList[PersistentEntityResource]())
  }
}
