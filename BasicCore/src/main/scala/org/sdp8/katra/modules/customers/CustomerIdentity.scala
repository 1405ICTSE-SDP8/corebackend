package org.sdp8.katra.modules.customers

import com.fasterxml.jackson.annotation.{JsonProperty, JsonSubTypes}
import com.fasterxml.jackson.databind.annotation.JsonSerialize

import scala.beans.BeanProperty


case class CustomerIdentity(@JsonProperty("idtype")@BeanProperty idtype: String,
                       @JsonProperty("ident")@BeanProperty ident: String) {
  override def toString: String = {
    StringBuilder.newBuilder
      .append(idtype)
      .append(':')
      .append(ident)
      .toString()
  }
}

object CustomerIdentity {
  def parse(str: String): CustomerIdentity = {
    val strs = str.split(':')
    if (strs.length < 2)
      throw new IllegalArgumentException(String.format("Invalid identity string format: %s", str))
    val idtype = strs(0)
    val id = strs.view(1,strs.length - 1).foldLeft("")(_.concat(_))
    new CustomerIdentity(idtype, id)
  }
}
