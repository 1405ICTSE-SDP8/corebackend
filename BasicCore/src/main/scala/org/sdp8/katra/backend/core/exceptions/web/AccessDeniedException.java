package org.sdp8.katra.backend.core.exceptions.web;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.FORBIDDEN)
public class AccessDeniedException extends RuntimeException {
    public AccessDeniedException() {}
    public AccessDeniedException(Throwable cause) {
        super(cause);
    }
}
