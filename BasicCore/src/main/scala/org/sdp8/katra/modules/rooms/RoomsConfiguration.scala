package org.sdp8.katra.modules.rooms

import javax.annotation.PostConstruct

import org.sdp8.katra.modules.rooms.entities.{Room, RoomNotAvailable}
import org.sdp8.katra.modules.rooms.repositories.RoomRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.{ComponentScan, Configuration}
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories


@Configuration
@ComponentScan
@EnableMongoRepositories(basePackageClasses = Array(classOf[RoomRepository]))
class RoomsConfiguration {

  @Autowired
  val test: RoomRepository = null

  @PostConstruct
  def testdef(): Unit ={
    test.save(new Room("2",0,"InvalidRoomType",RoomNotAvailable("test","")))
  }
}
