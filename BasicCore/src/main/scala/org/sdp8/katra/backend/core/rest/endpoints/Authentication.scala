package org.sdp8.katra.backend.core.rest.endpoints

import java.time.Instant
import java.time.temporal.ChronoUnit
import java.util.{Date, Optional}

import com.fasterxml.jackson.annotation.JsonInclude.Include
import com.fasterxml.jackson.annotation.{JsonInclude, JsonProperty}
import io.jsonwebtoken.{JwtBuilder, Jwts, SignatureAlgorithm}
import org.apache.http.HttpHeaders
import org.sdp8.katra.backend.api.security.{IUser, IUserManager}
import org.sdp8.katra.backend.core.exceptions.web.AccessDeniedException
import org.sdp8.katra.backend.core.security.tokens.TokenHandler
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.rest.webmvc.BasePathAwareController
import org.springframework.security.authentication.{AuthenticationManager, UsernamePasswordAuthenticationToken}
import org.springframework.security.core.AuthenticationException
import org.springframework.stereotype.Service
import org.springframework.web.bind.annotation._

import scala.annotation.meta.field

@Service("KatraAuthenticationController")
@BasePathAwareController
@RequestMapping(value = Array("/auth"))
class Authentication {
  @Autowired
  val authManager: AuthenticationManager = null

  @Autowired
  val userManager: IUserManager = null

  @Autowired
  val tokenHandler: TokenHandler = null

  @RequestMapping(value = Array("login"), method = Array(RequestMethod.POST, RequestMethod.GET))
  @ResponseBody
  def login(  @RequestParam username: String,
              @RequestParam password: String): TokenResponse = {
    val authReq = new UsernamePasswordAuthenticationToken(username, password)
    try {
      val result = authManager.authenticate(authReq)
      if (!result.isAuthenticated)
        throw new AccessDeniedException
      val expiry = Instant.now().plus(1, ChronoUnit.HOURS)
      val token = tokenHandler.createToken(result, Optional.of(expiry))
      val user = result.getPrincipal.asInstanceOf[IUser]

      //Renewal Token
      val refExp = expiry.plus(1, ChronoUnit.DAYS)
      val refToken = Jwts.builder()
        .setSubject(result.getName)
        .setIssuedAt(Date.from(Instant.now()))
        .setExpiration(Date.from(refExp))
        .signWith(SignatureAlgorithm.HS512, user.getRenewalTokenKey)
        .compact()
      TokenResponse(token, expiry.toString, refToken, refExp.toString)
    }
    catch {
      case e: AuthenticationException => throw new AccessDeniedException(e)
    }
  }

  @RequestMapping(value = Array("renewToken"), method = Array(RequestMethod.POST))
  @ResponseBody
  def renewToken( @RequestParam("username") username: String,
                  @RequestParam("reftoken") refTokenStr: String): TokenResponse = {
    val user = userManager.getUser(username)
    if (!user.isPresent)
      throw new AccessDeniedException()
    val refToken = Jwts.parser()
      .setSigningKey(user.get().getRenewalTokenKey)
      .parseClaimsJws(refTokenStr)

    if (refToken.getBody.getSubject != user.get.getUsername)
      throw new AccessDeniedException()

    val tokenExp = Instant.now().plus(1, ChronoUnit.HOURS)
    val token = tokenHandler.createToken(user.get, Optional.of(tokenExp))

    if (refToken.getBody.getExpiration.toInstant.until(Instant.now(), ChronoUnit.HOURS) > -1) {
      val refExp = tokenExp.plus(1, ChronoUnit.DAYS)
      val refToken = Jwts.builder()
        .setSubject(user.get.getUsername)
        .setIssuedAt(Date.from(Instant.now()))
        .setExpiration(Date.from(refExp))
        .signWith(SignatureAlgorithm.HS512, user.get.getRenewalTokenKey)
        .compact()
      return TokenResponse(token, tokenExp.toString, refToken, refExp.toString)
    }
    TokenResponse(token, tokenExp.toString, null, null)
  }
}

@JsonInclude(value = Include.NON_NULL)
case class TokenResponse(@(JsonProperty @field)("token") token: String,
                         @(JsonProperty @field)("expiry") expiry: String,
                         @(JsonProperty @field)("refToken") renewToken: String,
                         @(JsonProperty @field)("refTokenExpiry") renewTokenExpiry: String)