package org.sdp8.katra.modules.users.components;

import org.sdp8.katra.modules.users.entities.User;
import org.springframework.data.rest.webmvc.RepositoryLinksResource;
import org.springframework.hateoas.*;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.stereotype.Component;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@Component
public class UserResourceProcessor implements ResourceProcessor<Resources<User>> {
    @Override
    public Resources<User> process(Resources<User> resource) {
        Class<UsersController> clazz = UsersController.class;
        Link link = ControllerLinkBuilder
                .linkTo(clazz, methodOn(clazz).getCurrentUser(null,null))
                .withRel("currentUser");
        resource.add(link);
        return resource;
    }
}
