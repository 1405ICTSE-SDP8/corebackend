package org.sdp8.katra.modules.customers.entities;

import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.sdp8.katra.backend.util.ISO2LocalDate;
import org.sdp8.katra.backend.util.LocalDate2ISO;
import org.sdp8.katra.modules.customers.CustomerIdentity;
import org.sdp8.katra.modules.customers.CustomerIdentity$;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDate;
import java.util.Date;

@Document(collection = "Customers")
@JsonDeserialize(using = JsonDeserializer.None.class)
public final class Person extends Customer {

    private String gender;

    @JsonSerialize(converter = LocalDate2ISO.class)
    @JsonDeserialize(converter = ISO2LocalDate.class)
    private LocalDate dob;

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public LocalDate getDob() {
        return dob;
    }

    public void setDob(LocalDate dob) {
        this.dob = dob;
    }

    public static PersonBuilder builder() {
        return new PersonBuilder();
    }

    @Override
    public boolean isOrganization() {
        return false;
    }
}

class PersonBuilder {
    private Person cust = new Person();

    public PersonBuilder name(String name) {
        cust.setName(name);
        return this;
    }

    public PersonBuilder gender(String gender) {
        cust.setGender(gender);
        return this;
    }

    public PersonBuilder address(String addr) {
        cust.setAddress(addr);
        return this;
    }

    public PersonBuilder dob(LocalDate dob) {
        cust.setDob(dob);
        return this;
    }

    public PersonBuilder dob(Date dob) {
        cust.setDob(LocalDate.from(dob.toInstant()));
        return this;
    }

    public PersonBuilder identifier(String id) {
        cust.setIdentifier(CustomerIdentity$.MODULE$.parse(id));
        return this;
    }

    public PersonBuilder identifier(CustomerIdentity id) {
        cust.setIdentifier(id);
        return this;
    }

    public Person build() {
        return cust;
    }
}
