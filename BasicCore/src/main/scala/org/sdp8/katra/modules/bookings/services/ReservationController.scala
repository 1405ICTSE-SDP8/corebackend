package org.sdp8.katra.modules.bookings.services

import java.time.Instant

import org.sdp8.katra.modules.bookings.repositories.ReservationRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.rest.webmvc.RepositoryRestController
import org.springframework.http.{HttpHeaders, HttpStatus, ResponseEntity}
import org.springframework.web.bind.annotation.{RequestHeader, RequestMapping, RequestMethod, RequestParam}

@RepositoryRestController
@RequestMapping(path = Array("reservations"))
class ReservationController {

  @Autowired
  val repo: ReservationRepository = null;

  @RequestMapping(Array("checkAvailability"))
  def checkAvailability(@RequestHeader header: HttpHeaders,
                       @RequestParam("start") start: String,
                       @RequestParam("end") end: String): Unit = {

  }
}
