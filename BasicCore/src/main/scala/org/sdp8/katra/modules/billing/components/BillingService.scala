package org.sdp8.katra.modules.billing.components

import java.time.temporal.ChronoUnit
import java.util

import org.sdp8.katra.modules.billing.entities.{Invoice, InvoicedItem, Payment}
import org.sdp8.katra.modules.billing.repositories.{InvoiceRepository, PaymentRepository}
import org.sdp8.katra.modules.bookings.entities.{Reservation, Stay}
import org.sdp8.katra.modules.bookings.repositories.StayRepository
import org.sdp8.katra.modules.rooms.entities.RoomType
import org.sdp8.katra.modules.rooms.repositories.{RoomRepository, RoomTypeRepository}
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

import scala.collection.JavaConverters._
import scala.collection.mutable
import scala.collection.immutable

/**
  * BillingService
  * Depends on:
  * - Bookings module
  * - Rooms module
  */
@Service
class BillingService {
  @Autowired
  val staysRepo: StayRepository = null
  @Autowired
  val rtRepo: RoomTypeRepository = null
  @Autowired
  val roomRepo: RoomRepository = null
  @Autowired
  val invoiceRepo: InvoiceRepository = null
  @Autowired
  val paymentRepo: PaymentRepository = null

  /**
    * Generate all invoices for a Reservation
    *
    * @param rsvp   Reservation to generate for
    * @param dryRun If true, invoices will not be stored.
    * @return
    */
  def generateInvoicesFromReservation(rsvp: Reservation, dryRun: Boolean): java.util.List[Invoice] = {
    val stays = staysRepo.findAllByReservationId(rsvp.getId)
    val roomTypes = new util.HashMap[String, RoomType]

    val roomInv = new Invoice
    roomInv.setCustomerId(rsvp.getCustomerId)
    roomInv.setReservationId(rsvp.getId)
    //Pass 1, Generate Invoices
    val out = stays.asScala.toStream
      .flatMap {s =>
        val output: mutable.Buffer[Invoice] = new mutable.ListBuffer[Invoice]
        val room = roomRepo.findOne(s.roomId)
        val rt: RoomType = roomTypes.computeIfAbsent(room.getRoomTypeId, rtRepo.findOne)

        roomInv.getItems.add(new InvoicedItem(s"Room: ${rt.getName}", calculateStayCost(s), 1))
        if (rsvp.isSeperateBills) {
          val inv = new Invoice
          inv.setCustomerId(s.getCustomerId)
          inv.getItems.addAll(s.getCharges)
          inv.setStayId(s.getId)
          inv.setReservationId(rsvp.getId)
          output += inv
        }
        else roomInv.getItems.addAll(s.getCharges.asScala.iterator.map { c =>
          c.setItem(s"Room ${room.prefix}-${room.number}: ${c.getItem}"); c
        }.toList.asJava)

        output
      }
      .++(Option(roomInv))
      .groupBy {
        _.getCustomerId
      }
      .valuesIterator
      .map(_.reduceLeft[Invoice] { (a, b) =>
        a.getItems.addAll(b.getItems)
        a
      })
      .map(invoiceItemDedupe)
      .toList.asJava

    if (!dryRun) invoiceRepo.save(out)
    else out
  }

  /**
    * Generate invoice for a stay
    *
    * @param stay   Stay to generate for.
    * @param dryRun If true, invoice will not be stored.
    * @return
    */
  def generateInvoiceForStay(stay: Stay, dryRun: Boolean): Invoice = {
    val roomType = rtRepo.findOne(roomRepo.findOne(stay.getRoomId).getRoomTypeId)
    val inv = new Invoice
    inv.setCustomerId(stay.getCustomerId)
    inv.getItems.add(new InvoicedItem(s"Room: ${roomType.getName}", calculateStayCost(stay), 1))
    inv.getItems.addAll(stay.getCharges)
    inv.setStayId(stay.getId)
    invoiceItemDedupe(inv)
    if (!dryRun)
      invoiceRepo.save(inv)
    else
      inv
  }

  /**
    * Generates payment for invoices
    * @param invoices Invoices to pay
    * @return
    */
  def generatePayment(dryRun: Boolean, 
                      transId: String,
                      method: String,
                      amount: Double,
                      invoices: Invoice*): Payment = {
    val total = invoices.toStream.map(_.getTotal).sum
    if (amount != total)
      throw new IllegalArgumentException("Amount must match sum of invoices!")
    val payment = new Payment()
    payment.setAmount(amount)
    payment.setInvoices(invoices.toStream.map(_.getId).toList.asJava)
    payment.setMethod(method)
    payment.setTransId(transId)
    if (dryRun) payment else paymentRepo.save(payment)
  }

  private def invoiceItemDedupe(invoice: Invoice): Invoice = {
    invoice.setItems(invoice.getItems.asScala
      .groupBy(item => (item.getItem, item.getUnitPrice))
      .par.valuesIterator
      .map(_.reduceLeft[InvoicedItem] { (i1, i2) =>
        i1.setAmount(i1.getAmount + i2.getAmount)
        i1
      })
      .toList.asJava
    )
    invoice
  }

  private def calculateStayCost(stay: Stay): Double = {
    val hours = ChronoUnit.HOURS.between(stay.getStartTime, stay.getExpectedEndTime)
    if (hours < 0)
      throw new IllegalStateException("Duration of stay cannot be negative!")
    hours * stay.getRate
  }
}
