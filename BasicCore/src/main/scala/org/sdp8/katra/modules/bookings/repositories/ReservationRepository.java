package org.sdp8.katra.modules.bookings.repositories;

import org.sdp8.katra.modules.bookings.entities.Reservation;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.time.Instant;

@RepositoryRestResource(path = "reservations")
public interface ReservationRepository extends MongoRepository<Reservation, String> {

    @Query(value = "{'$and':[{'checkInDate':{'$lte':?0}},{'checkOutDate':{'$gte':?0}}]}", count = true)
    long countAtTime(@Param("t") Instant instant);

    @Query(value = "{'$and':[{'checkInDate':{'$lte':?0}},{'checkOutDate':{'$gte':?0}}]}")
    Iterable<Reservation> findAllAtTime(@Param("t") Instant instant);

    @Query(value = "{'$and':[{'checkInDate':{'$gte':?0}},{'checkOutDate':{'$lte':?1}}]}", count = true)
    long countAllOverlapping(@Param("start") Instant start, @Param("end") Instant end);

    @Query(value = "{'$and':[{'checkInDate':{'$gte':?0}},{'checkOutDate':{'$lte':?1}}]}")
    Iterable<Reservation> findAllOverlapping(@Param("start") Instant start, @Param("end") Instant end);
}
