package org.sdp8.katra.backend.core.security

import org.sdp8.katra.backend.api.security.IUserManager
import org.sdp8.katra.backend.core.security.tokens.TokenAuthenticationService
import org.sdp8.katra.modules.users.services.UserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.ApplicationListener
import org.springframework.context.annotation.{Bean, Configuration}
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.event.AbstractAuthenticationFailureEvent
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.{EnableWebSecurity, WebSecurityConfigurerAdapter}
import org.springframework.security.core.userdetails.{UserDetailsService => IUserDetailsService}
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter
import org.springframework.stereotype.Component

@Component
@EnableWebSecurity
@Configuration
@EnableGlobalMethodSecurity(prePostEnabled=true)
class SecurityConfig extends WebSecurityConfigurerAdapter(true) {

  @Autowired
  private val userService: IUserManager = null
  @Autowired
  private val tokenAuthenticationService: TokenAuthenticationService = null
  @Autowired
  private val passwordEncoderVal: PasswordEncoder = null

  override def configure(auth: AuthenticationManagerBuilder): Unit = {
    if (userService == null) throw new Exception
    auth.userDetailsService(userService).passwordEncoder(this.passwordEncoderVal)
  }
  override def configure(http: HttpSecurity): Unit = {
    http
      .exceptionHandling().and()
      .anonymous().and()
      .servletApi().and()
      .headers().cacheControl().and()

    http
      .csrf().disable()
      .authorizeRequests()
      .antMatchers("/api/auth/**").permitAll()
      .anyRequest().authenticated().and()
      .addFilterBefore(new StatelessAuthenticationFilter(tokenAuthenticationService),
        classOf[UsernamePasswordAuthenticationFilter])
  }

  @Bean
  def passwordEncoder: PasswordEncoder = new BCryptPasswordEncoder

  @Bean
  override def authenticationManagerBean(): AuthenticationManager = {
    super.authenticationManagerBean()
  }
}
