package org.sdp8.katra.modules.billing.entities;

public class ExtraCharge {

    private ChargeType type;
    private double modifier;

    public ChargeType getType() {
        return type;
    }

    public void setType(ChargeType type) {
        this.type = type;
    }

    public double getModifier() {
        return modifier;
    }

    public void setModifier(double modifier) {
        this.modifier = modifier;
    }

    public enum ChargeType {
        ABSOLUTE,
        FRACTIONAL
    }
}
