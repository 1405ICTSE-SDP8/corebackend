package org.sdp8.katra.backend.core.security

import java.util.UUID
import javax.annotation.PostConstruct

import org.apache.log4j.Logger
import org.sdp8.katra.backend.api.security.IUserManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Component
class InitialUserCreator {

  @Autowired
  val userman: IUserManager = null

  @PostConstruct
  def createInitialUser(): Unit = {
    if (userman.countUsers() == 0) {
      val username = "admin"
      val password = "admin"
      userman.saveUser(userman.getUserBuilder
        .username(username)
        .password(password)
        .enabled(true)
        .role("superadmin")
        .build()
      )
      Logger.getLogger(classOf[InitialUserCreator])
        .info(String.format("Initial user created: %s, password: %s", username, password))
    }
  }
}
