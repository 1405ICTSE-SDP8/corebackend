package org.sdp8.katra.modules.facilities;

import org.sdp8.katra.modules.facilities.repositories.FacilityRepository;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@Configuration
@ComponentScan
@EnableMongoRepositories(basePackageClasses = {FacilityRepository.class})
public class FacilityConfiguration {
}
