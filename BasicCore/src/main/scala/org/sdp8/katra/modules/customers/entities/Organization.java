package org.sdp8.katra.modules.customers.entities;

import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "Customers")
@JsonDeserialize(using = JsonDeserializer.None.class)
public final class Organization extends Customer {
    @Override
    public boolean isOrganization() {
        return true;
    }
}
