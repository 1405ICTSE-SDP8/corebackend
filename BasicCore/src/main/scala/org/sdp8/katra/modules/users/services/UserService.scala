package org.sdp8.katra.modules.users.services

import java.time.Instant
import java.util
import java.util.Optional
import javax.annotation.PostConstruct

import com.fasterxml.jackson.annotation.JsonSetter
import org.sdp8.katra.backend.api.security.{IUser, IUserManager, UserBuilder}
import org.sdp8.katra.backend.core.exceptions.data.DuplicateDataException
import org.sdp8.katra.modules.users.entities.User
import org.sdp8.katra.modules.users.repositories.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.core.env.Environment
import org.springframework.data.mongodb.core.mapping.MongoPersistentEntity
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Service

@Service
class UserService extends IUserManager {
  @Autowired
  val userRepository: UserRepository = null
  @Autowired
  val passwordEncoder: PasswordEncoder = null
  @Autowired
  val env: Environment = null
  @Autowired
  val roles: RoleService = null

  @PostConstruct
  def postInit(): Unit = {
    User.pwe = passwordEncoder
    User.repo = userRepository
    User.roleServ = roles
  }

  override def loadUserByUsername(username: String): User = {
    val user = Option(userRepository.findByUsername(username))
    if (user.isEmpty)
      throw new UsernameNotFoundException(username)
    user.get
  }

  override def saveUser(user: IUser): Unit = {
      user match {
        case user2: User => userRepository.save(user2)
        case _ => userRepository.save(new User(user))
      }
  }

  override def addUser(user: IUser, password: String): Unit = {
    if (userRepository.findByUsername(user.getUsername) == null) {
      user.setPassword(passwordEncoder.encode(password))
      saveUser(user)
    }
    else
      throw new IllegalStateException("Attempt to add duplicate user", new DuplicateDataException)
  }

  override def getUser(username: String): Optional[User] = {
    Optional.ofNullable(userRepository.findByUsername(username))
  }

  override def removeUser(username: String): Unit = Option(userRepository.findByUsername(username)) foreach { u =>
    userRepository.delete(u)
  }

  override def setPassword(username: String, password: String): Unit =
    Option(userRepository.findByUsername(username)) foreach {
      _.setPassword(password)
    }

  override def incrementUserFailureCount(username: String): Unit = getUser(username) ifPresent { u =>
    u.setFailureCount(u.getFailureCount + 1)
  }

  override def resetUserFailureCount(username: String): Unit = getUser(username) ifPresent { u =>
    u.setFailureCount(0)
  }

  override def countUsers(): Long = userRepository.count()

  override def getUserBuilder: UserBuilder = new UserBuilderImpl(passwordEncoder)
}

private class UserBuilderImpl(private val pwe: PasswordEncoder) extends UserBuilder{
  private var username: Option[String] = None
  private var password: Option[String] = None
  private var pwexpiry: Option[Instant] = None
  private var role: Option[String] = None
  private var permissions: util.List[String] = new util.LinkedList[String]
  private var accexpiry: Option[Instant] = None
  private var enabled: Option[Boolean] = None

  override def username(username: String): UserBuilder = {
    this.username = Option(username)
    this
  }

  override def passwordExpiry(instant: Instant): UserBuilder = {
    this.pwexpiry = Option(instant)
    this
  }

  override def role(role: String): UserBuilder = {
    this.role = Option(role)
    this
  }

  override def permissions(perms: util.Collection[String]): UserBuilder = {
    perms.forEach(p => permissions.add(p))
    this
  }

  override def permissions(perms: String*): UserBuilder = {
    perms.foreach(p => permissions.add(p))
    this
  }

  override def accountExpiry(instant: Instant): UserBuilder = {
    this.accexpiry = Option(instant)
    this
  }

  /**
    * Sets a password for the user using the active PasswordEncoder
    * @param password Plain Text Password
    * @return This builder
    */
  override def password(password: String): UserBuilder = {
    this.password = Option(password)
    this
  }

  override def addPermission(perm: String): UserBuilder = {
    this.permissions.add(perm)
    this
  }

  override def enabled(flag: Boolean): UserBuilder = {
    this.enabled = Option(flag)
    this
  }

  override def build(): IUser = {
    val user = new User(username.get)
    password.foreach(p => user.setPassword(p))
    role.foreach(p => user.setRole(p))
    enabled.foreach(p => user.setEnabled(p))
    pwexpiry.foreach(p => user.setPasswordExpiry(p))
    accexpiry.foreach(p => user.setAccountExpiry(p))
    permissions.forEach(p => user.addPermissions(p))
    user
  }
}

