package org.sdp8.katra.modules.checking;

import org.sdp8.katra.backend.core.exceptions.web.NotFoundException;
import org.sdp8.katra.modules.bookings.entities.Reservation;
import org.sdp8.katra.modules.bookings.entities.RoomReservationItem;
import org.sdp8.katra.modules.bookings.repositories.ReservationRepository;
import org.sdp8.katra.modules.customers.entities.Customer;
import org.sdp8.katra.modules.customers.repositories.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.PersistentEntityResource;
import org.springframework.data.rest.webmvc.PersistentEntityResourceAssembler;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

@RepositoryRestController
public class CheckingController {
    @Autowired
    ReservationRepository rsvp;
    @Autowired
    CustomerRepository custRepo;
    @Autowired
    CheckingService checkServ;

    @RequestMapping("checkIn")
    @ResponseBody
    public List<PersistentEntityResource> checkIn(String rsvpId, PersistentEntityResourceAssembler res) {
        return checkServ.checkIn(rsvp.findOne(rsvpId)).stream().map(res::toResource).collect(Collectors.toList());
    }

    @RequestMapping("groupCheckIn")
    @ResponseBody
    public List<PersistentEntityResource> checkIn(@RequestBody GroupCheckInRequest req, PersistentEntityResourceAssembler res) {
        Reservation rsvp = this.rsvp.findOne(req.rsvpId);
        Map<RoomReservationItem, Customer> custMap = new HashMap<>();
        rsvp.getRooms().stream().forEach(i -> {
            int count = i.getAmount();
            while (count-- > 0) {
                String custId = req.getCustomerMap().get(i.getRoomTypeId()).remove(0);
                if (custId != null)
                    custMap.put(i, custRepo.findOne(custId));
                else
                    throw new NoSuchElementException();
            }
        });
        return checkServ.checkIn(rsvp, custMap).stream().map(res::toResource).collect(Collectors.toList());
    }

    @RequestMapping("checkOut")
    @ResponseBody
    public boolean checkOut(String rsvpId) {
        checkServ.checkOut(rsvp.findOne(rsvpId));
        return true;
    }

    public class GroupCheckInRequest {
        private String rsvpId;
        private Map<String,List<String>> customerMap;

        public String getRsvpId() {
            return rsvpId;
        }

        public void setRsvpId(String rsvpId) {
            this.rsvpId = rsvpId;
        }

        public Map<String, List<String>> getCustomerMap() {
            return customerMap;
        }

        public void setCustomerMap(Map<String, List<String>> customerMap) {
            this.customerMap = customerMap;
        }
    }
}
