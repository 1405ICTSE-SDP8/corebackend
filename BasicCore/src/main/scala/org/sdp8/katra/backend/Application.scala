package org.sdp8.katra.backend

import com.fasterxml.jackson.databind.Module
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.context.annotation.Bean
import org.springframework.data.rest.core.config.RepositoryRestConfiguration
import org.springframework.data.rest.core.mapping.RepositoryDetectionStrategy.RepositoryDetectionStrategies
import org.springframework.data.rest.webmvc.config.{RepositoryRestConfigurer, RepositoryRestConfigurerAdapter}

@SpringBootApplication
class Application {
  @Bean
  def repositoryRestConfigurer(): RepositoryRestConfigurer = {
    new RepositoryRestConfigurerAdapter() {
      override def configureRepositoryRestConfiguration(config: RepositoryRestConfiguration): Unit = {
        config.setReturnBodyOnCreate(true)
        config.setReturnBodyOnUpdate(true)
        config.setRepositoryDetectionStrategy(RepositoryDetectionStrategies.ANNOTATED)
        config.setBasePath("/api")
      }
    }
  }

  @Bean
  def scalaJacksonModule: Module = DefaultScalaModule
  @Bean
  def jsr310JacksonModule: Module = new JavaTimeModule
}
