package org.sdp8.katra.backend.api.security;

import java.time.Instant;
import java.util.Collection;

public abstract class UserBuilder {
    abstract public UserBuilder username(String username);
    abstract public UserBuilder password(String password);
    abstract public UserBuilder role(String role);
    abstract public UserBuilder permissions(Collection<String> perms);
    abstract public UserBuilder permissions(String... perms);
    abstract public UserBuilder addPermission(String perm);
    abstract public UserBuilder accountExpiry(Instant instant);
    abstract public UserBuilder passwordExpiry(Instant instant);
    abstract public UserBuilder enabled(boolean flag);
    abstract public IUser build();
}
