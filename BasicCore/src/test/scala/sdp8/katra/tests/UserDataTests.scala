package sdp8.katra.tests

import org.junit.Assert._
import org.junit.Test
import org.junit.runner.RunWith
import org.sdp8.katra.backend.Application
import org.sdp8.katra.backend.api.security.IUserManager
import org.sdp8.katra.modules.datastore.hybrid.DBConfiguration
import org.sdp8.katra.modules.users.UsersConfiguration
import org.sdp8.katra.modules.users.entities.User
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.core.env.Environment
import org.springframework.security.authentication.{AuthenticationManager, BadCredentialsException, UsernamePasswordAuthenticationToken}
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner

@RunWith(classOf[SpringJUnit4ClassRunner])
@ContextConfiguration(classes = Array(classOf[Application],
  classOf[DBConfiguration],
  classOf[UsersConfiguration]))
class UserDataTests {
  @Autowired
  val userManager: IUserManager = null
  @Autowired
  val env: Environment = null
  @Autowired
  val auth: AuthenticationManager = null

  @Test
  def testUserData(): Unit = {
    val user = new User("testUser00")
    user.setEnabled(true)
    user.setRole("TEST")
    user.addPermissions("katra.test")
    userManager.addUser(user, "testPassword00")
  }

  @Test
  def testValidAuthentication(): Unit = {
    val user = userManager.getUserBuilder
      .username("testUser01")
      .password("testPassword01")
      .enabled(true)
      .role("TEST")
      .addPermission("katra.test")
      .build()
    userManager.saveUser(user)
    val authCreds = new UsernamePasswordAuthenticationToken("testUser01","testPassword01")
    val result = auth.authenticate(authCreds)
    assertTrue(result.isAuthenticated)
  }

  @Test(expected = classOf[BadCredentialsException])
  def testInvalidAuthentication(): Unit = {
    val user = userManager.getUserBuilder
      .username("testUser02")
      .password("testPassword02")
      .enabled(true)
      .role("TEST")
      .build()

    userManager.saveUser(user)
    val authCreds = new UsernamePasswordAuthenticationToken("testUser02","wrongPassword")
    auth.authenticate(authCreds)
  }
}
