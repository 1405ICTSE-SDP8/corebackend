package org.sdp8.katra.modules.billing;

import org.sdp8.katra.modules.billing.repositories.InvoiceRepository;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@Configuration
@ComponentScan
@EnableMongoRepositories(basePackageClasses = {InvoiceRepository.class})
public class BillingConfiguration {
}
