package org.sdp8.katra.backend.core.rest

import org.sdp8.katra.backend.api.ValidatorRegistrationAgent
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.context.embedded.FilterRegistrationBean
import org.springframework.context.annotation.{Bean, Configuration}
import org.springframework.data.rest.core.event.ValidatingRepositoryEventListener
import org.springframework.data.rest.webmvc.config.RepositoryRestMvcConfiguration
import org.springframework.web.cors.{CorsConfiguration, UrlBasedCorsConfigurationSource}
import org.springframework.web.filter.CorsFilter

@Configuration
class RestConfiguration extends RepositoryRestMvcConfiguration{

  @Autowired(required = false)
  val vRegAgs: Array[ValidatorRegistrationAgent] = Array()

  override def configureValidatingRepositoryEventListener(v: ValidatingRepositoryEventListener): Unit = {
    vRegAgs foreach {_.registerValidators(v)}
  }

  @Bean
  def corsFilterRegistration: FilterRegistrationBean = {
    val source = new UrlBasedCorsConfigurationSource
    val config = new CorsConfiguration

    config.setAllowCredentials(true)
    config.addAllowedOrigin("*")
    config.addAllowedHeader("*")
    config.addAllowedMethod("OPTIONS")
    config.addAllowedMethod("HEAD")
    config.addAllowedMethod("GET")
    config.addAllowedMethod("PUT")
    config.addAllowedMethod("POST")
    config.addAllowedMethod("DELETE")
    config.addAllowedMethod("PATCH")
    source.registerCorsConfiguration("/**", config)

    val bean = new FilterRegistrationBean(new CorsFilter(source))
    bean.setOrder(org.springframework.core.Ordered.HIGHEST_PRECEDENCE)
    bean
  }



}
