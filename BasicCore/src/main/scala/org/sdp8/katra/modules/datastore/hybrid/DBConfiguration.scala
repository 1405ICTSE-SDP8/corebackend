package org.sdp8.katra.modules.datastore.hybrid

import java.util

import com.mongodb._
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.{Bean, ComponentScan, Configuration}
import org.springframework.core.env.Environment
import org.springframework.data.mongodb.MongoDbFactory
import org.springframework.data.mongodb.config.AbstractMongoConfiguration
import org.springframework.data.mongodb.core.{MongoClientFactoryBean, MongoClientOptionsFactoryBean, MongoFactoryBean, MongoTemplate}
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories
import org.springframework.data.mongodb.repository.support.MongoRepositoryFactory

import scala.collection.mutable

@Configuration
@EnableMongoRepositories(basePackages = Array("org.sdp8.katra.modules.datastore.hybrid.repositories"))
@ComponentScan
class DBConfiguration extends AbstractMongoConfiguration{
  @Autowired
  val env: Environment = null

  @Autowired
  val mongofac: MongoClientFactoryBean = null

  //TODO: Add support for connection options
  @Bean
  override def mongo(): Mongo = {
    mongofac.getObject
  }

  @Bean
  def mongoClientFactory(): MongoClientFactoryBean = {
    val fac = new MongoClientFactoryBean
    fac.setHost(env.getRequiredProperty("mongo.host"))
    fac.setPort(Integer.parseInt(env.getRequiredProperty("mongo.port")))

    val cred = for {
      u <- Option(env.getProperty("mongo.user"))
      p <- Option(env.getProperty("mongo.port"))
    } yield {
      val list = new mutable.ListBuffer[MongoCredential]
      list += MongoCredential.createCredential(u, getDatabaseName, p.toCharArray)
    }

    cred foreach {c => fac.setCredentials(c.toArray)}

    val optionsFactory = new MongoClientOptionsFactoryBean
    optionsFactory.setSingleton(false)
    optionsFactory.setWriteConcern(WriteConcern.ACKNOWLEDGED)

    fac.setMongoClientOptions(optionsFactory.getObject)

    fac
  }

  override def getDatabaseName: String = env.getProperty("mongo.db", "ProjectKatraDB")
}
