package org.sdp8.katra.modules.rooms.entities

import com.fasterxml.jackson.databind.annotation.{JsonDeserialize, JsonSerialize}
import org.sdp8.katra.modules.rooms.components.{RoomStatusJsonDeserializer, RoomStatusJsonSerializer}
import org.springframework.data.mongodb.core.mapping.Field

import scala.annotation.meta.field

@JsonSerialize(using = classOf[RoomStatusJsonSerializer])
@JsonDeserialize(using = classOf[RoomStatusJsonDeserializer])
abstract class RoomStatus(val status: String)
case class RoomOccupied(@(Field @field)("occupationId") occupationId: String)
  extends RoomStatus(RoomStatus.OCCUPIED)
case class RoomNotAvailable(@(Field @field)("reason") reason: String,
                            @(Field @field)("extendedReason")extendedReason: String = "")
  extends RoomStatus(RoomStatus.NOT_AVAILABLE)
case object RoomAvailable extends RoomStatus(RoomStatus.AVAILABLE)
case object UnknownStatus extends RoomStatus(RoomStatus.UNKNOWN)

object RoomStatus {
  val OCCUPIED = "occupied"
  val NOT_AVAILABLE = "unavailable"
  val AVAILABLE = "available"
  val UNKNOWN = "unknown"
}