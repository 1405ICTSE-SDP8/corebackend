package org.sdp8.katra.modules.users.components

import org.sdp8.katra.modules.users.repositories.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.rest.webmvc.{PersistentEntityResource, PersistentEntityResourceAssembler, RepositoryRestController}
import org.springframework.http.HttpStatus
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.security.core.Authentication
import org.springframework.web.bind.annotation._

@RepositoryRestController
@RequestMapping(path = Array("users"))
class UsersController {
  @Autowired
  val repo: UserRepository = null

  @RequestMapping(path = Array("getCurrent"), method = Array(RequestMethod.GET))
  @ResponseBody
  def getCurrentUser(auth: Authentication, res: PersistentEntityResourceAssembler): PersistentEntityResource = {
    res.toResource(repo.findByUsername(auth.getName))
  }

  @RequestMapping(path = Array("{id}/resetRefTokens"), method = Array(RequestMethod.GET))
  @PreAuthorize("(authentication.principal.getUID() == #id) OR hasAuthority('PERM_katra.users.resetRefTokens')")
  @ResponseBody
  @ResponseStatus(HttpStatus.NO_CONTENT)
  def resetRefTokens(@PathVariable("id") id: String): Unit = {
    repo.findOne(id).resetRenewalTokenKey()
  }
}
