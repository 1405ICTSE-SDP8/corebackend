package org.sdp8.katra.modules.rooms.entities

import com.fasterxml.jackson.annotation.JsonGetter
import org.springframework.data.mongodb.core.mapping.Document

import scala.beans.BeanProperty
import org.springframework.data.annotation.{Id, Version}

@Document(collection = "Rooms")
class Room(@BeanProperty val prefix: String,
           @BeanProperty val number: Short,
           @BeanProperty val roomTypeId: String,
           @BeanProperty var roomStatus: RoomStatus) {
  @Id @BeanProperty
  val id: String = null;

  @BeanProperty
  var enabled = true

  @Version
  private val version: Long = 0

  @JsonGetter
  private def getRoomId = this.getId

  def getVersion = version;
}