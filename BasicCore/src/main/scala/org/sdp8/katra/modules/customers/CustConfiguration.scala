package org.sdp8.katra.modules.customers

import org.sdp8.katra.modules.customers.repositories.CustomerRepository
import org.springframework.context.annotation.{ComponentScan, Configuration}
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories

@ComponentScan
@Configuration
@EnableMongoRepositories(basePackageClasses = Array(classOf[CustomerRepository]))
class CustConfiguration {

}
