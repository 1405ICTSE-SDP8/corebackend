package org.sdp8.katra.backend.api;

import org.springframework.data.rest.core.event.ValidatingRepositoryEventListener;

public interface ValidatorRegistrationAgent {
    void registerValidators(ValidatingRepositoryEventListener v);
}
