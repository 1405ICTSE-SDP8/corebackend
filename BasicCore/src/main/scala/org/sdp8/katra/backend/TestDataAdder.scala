package org.sdp8.katra.backend

import org.sdp8.katra.modules.billing.repositories.{InvoiceRepository, PaymentRepository}
import org.sdp8.katra.modules.bookings.repositories.{ReservationRepository, StayRepository}
import org.sdp8.katra.modules.customers.repositories.CustomerRepository
import org.sdp8.katra.modules.facilities.repositories.FacilityRepository
import org.sdp8.katra.modules.rooms.entities.RoomType
import org.sdp8.katra.modules.rooms.repositories.{RoomRepository, RoomTypeRepository}
import org.sdp8.katra.modules.users.repositories.UserRepository
import org.springframework.context.annotation.Bean

@Bean
class TestDataAdder {

  val userRepo: UserRepository = null

  val invRepo: InvoiceRepository = null
  val payRepo: PaymentRepository = null

  val rsvpRepo: ReservationRepository = null
  val stayRepo: StayRepository = null

  val custRepo: CustomerRepository = null

  val facRepo: FacilityRepository = null

  val roomRepo: RoomRepository = null
  val rtRepo: RoomTypeRepository = null


  def addTestData(): Unit = {
    val rt = new RoomType()
    rt.setBaseRate(10.0F)
    rt.setDescription("Basic Room Type")
    rt.setFacilities(new java.util.ArrayList[String])
  }
}
