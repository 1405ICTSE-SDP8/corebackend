package org.sdp8.katra.modules.customers.components

import org.sdp8.katra.modules.customers.api.ICustomerManager
import org.sdp8.katra.modules.customers.repositories.CustomerRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

//TODO: Delete if unused
@Service
class CustomersService extends ICustomerManager {
  @Autowired
  val repo: CustomerRepository = null


}
