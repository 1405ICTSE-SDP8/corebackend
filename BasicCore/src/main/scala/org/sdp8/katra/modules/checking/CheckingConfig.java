package org.sdp8.katra.modules.checking;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan
public class CheckingConfig {
}
