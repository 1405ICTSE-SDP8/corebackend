package org.sdp8.katra.backend.core.security.tokens;

import io.jsonwebtoken.*;
import io.jsonwebtoken.impl.compression.CompressionCodecs;
import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.codec.Utf8;
import org.springframework.security.oauth.common.signature.InvalidSignatureException;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.util.Date;
import java.util.Optional;


//TODO: Token Expiration.
@Component
public final class TokenHandler {

    private final byte[] secret;
    public static final String authScheme = "KATRA-TOKEN";
    private final SignatureAlgorithm alg = SignatureAlgorithm.HS512;

    @Autowired
    public TokenHandler(Environment env) {
        this.secret = Utf8.encode(env.getProperty("katra.token.secret", ""));
    }

    public Jws<Claims> parseToken(String tokenString) {
        Jws<Claims> token = Jwts.parser()
                .setSigningKey(secret)
                .parseClaimsJws(tokenString);
        if (token.getHeader().getAlgorithm().equals(alg.getValue()))
            return token;
        else
            throw new InvalidSignatureException("Token Algorithm does not match.");

    }

    public String createToken(UserDetails user, Optional<Instant> expiry) {
        return createToken(user.getUsername(), expiry);
    }

    public String createToken(Authentication auth, Optional<Instant> expiry) {
        if (auth.getPrincipal() instanceof UserDetails)
            return createToken((UserDetails) auth.getPrincipal(), expiry);
        else if (auth.getPrincipal() instanceof String)
            return createToken((String) auth.getPrincipal(), expiry);
        else
            throw new RuntimeException("Unable to get principle for token generation!");
    }

    private String createToken(String username, Optional<Instant> expiry) {
        JwtBuilder token = Jwts.builder()
                .setSubject(username)
                .signWith(alg, secret)
                .compressWith(CompressionCodecs.GZIP)
                .setIssuedAt(Date.from(Instant.now()));
        expiry.ifPresent(i -> token.setExpiration(Date.from(i)));
        return token.compact();
    }
}