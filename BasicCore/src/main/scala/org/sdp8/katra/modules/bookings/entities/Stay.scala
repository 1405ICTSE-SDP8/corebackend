package org.sdp8.katra.modules.bookings.entities

import java.time.Instant
import java.time.temporal.{Temporal, TemporalAdjusters, TemporalAmount, TemporalUnit}
import java.util

import org.sdp8.katra.modules.billing.entities.InvoicedItem
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document

import scala.beans.BeanProperty

@Document(collection = "RoomOccupations")
class Stay {

  @Id
  @BeanProperty
  val id: String = null
  @BeanProperty
  var customerId: String = null
  @BeanProperty
  var reservationId: String = null
  @BeanProperty
  var roomId: String = null
  @BeanProperty
  var startTime: Instant = null
  @BeanProperty
  var expectedEndTime: Instant = null
  /**
    * Hourly rate.
    */
  @BeanProperty
  var rate: Double = 0
  @BeanProperty
  val charges: util.List[InvoicedItem] = new util.LinkedList[InvoicedItem]()
}
