package org.sdp8.katra.backend.api.security;

import org.springframework.security.core.userdetails.UserDetails;
import scala.Int;

import java.time.Instant;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public interface IUser extends UserDetails {
    String getUID();
    String getUsername();

    /**
     * Gets the password, in encoded form if a encoder is used.
     * @return
     */
    String getPassword();
    String getRole();
    List<String> getPermissions();
    int getFailureCount();
    Optional<Instant> getAccountExpiry();
    Optional<Instant> getPasswordExpiry();
    Map<String, String> getCustomProperties();
    byte[] getRenewalTokenKey();

    void setRole(String role);
    void addPermissions(String... permissions);
    void removePermissions(String... permissions);
    void setFailureCount(int count);
    void setEnabled(boolean enabled);
    void setCustomProperties(Map<String, String> props);
    void resetRenewalTokenKey();
    /**
     * Sets the password, encoding it if required.
     * @param password
     */
    void setPassword(String password);

    /**
     * Sets the account expiry.
     * @param instant Instant of account expiry, may be null for no expiry.
     */
    void setAccountExpiry(Instant instant);

    /**
     * Sets the password expiry
     * @param instant Instant of password expiry, may be null for no expiry.
     */
    void setPasswordExpiry(Instant instant);
}

