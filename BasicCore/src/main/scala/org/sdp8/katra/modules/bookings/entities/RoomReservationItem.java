package org.sdp8.katra.modules.bookings.entities;

import com.fasterxml.jackson.annotation.JsonCreator;
import org.springframework.data.annotation.PersistenceConstructor;

public class RoomReservationItem {
    private String roomTypeId;
    private short amount;
    private double rate;

    @PersistenceConstructor
    @JsonCreator
    private RoomReservationItem() {}

    public RoomReservationItem(String roomTypeId, short amount, double rate) {
        this.roomTypeId = roomTypeId;
        this.amount = amount;
        this.rate = rate;
    }

    public String getRoomTypeId() {
        return roomTypeId;
    }

    public void setRoomTypeId(String roomTypeId) {
        this.roomTypeId = roomTypeId;
    }

    public short getAmount() {
        return amount;
    }

    public void setAmount(short amount) {
        this.amount = amount;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }
}
